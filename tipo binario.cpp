#include<iostream>
#include<fstream>
using namespace std;
struct Persona{
	char nombre[8];
	int edad;
};
/*proceso de escritura de la memoria hacia el archivo
1-a que punto de la memoria voy a pasar los bytes
2-cuantos bytes voy a pasar
*/
main() {
	ofstream salida;//salgo hacia el archivo y escribo
	ifstream entrada;//entro al archivo, accedo a �l y lo leo
	salida.open("data.bin",ios::binary);
	Persona p={"Gian",23};
	salida.write((char*)&p,sizeof(Persona));/*primer parametro: puntero tipo char que guarda el byte de inicio, segundo parametro: cuantos bytes voy a pasar*/
	salida.write((char*)&p,sizeof(Persona));
	salida.close();
}
